

import controller.Webserver;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.GuiHandler;

public class JavaFXApp extends Application
{
	
	
	 
    @Override
    public void start(Stage primaryStage)
    {
        GridPane root = new GridPane();
        
        
        Scene scene = new Scene(root);

        GuiHandler gui = new GuiHandler(root);
      
        primaryStage.setScene(scene);
        primaryStage.setTitle("Webserver");
        // disable close
        primaryStage.setOnCloseRequest(event -> event.consume());
        // primaryStage.setFullScreen(true);
        primaryStage.show();
        
    }

    @SuppressWarnings("deprecation")
	public static void main(String[] args)
    {
        launch(args);
    }

}