package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import view.GuiHandler;

public class Webserver extends Thread
{
	GuiHandler gui;
	
	public Webserver(GuiHandler gui)
	{
		this.gui = gui;
	}

	public void run()
	{
		try (ServerSocket serverSocket = new ServerSocket(8080))
		{
			while (true)
			{
				try (Socket client = serverSocket.accept())
				{
					handleClient(client);
				}
			}
		} catch (IOException ex)
		{

		}

	}

	private void handleClient(Socket client) throws IOException
	{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));

		StringBuilder requestBuilder = new StringBuilder();
		String line;
		while (!(line = br.readLine()).isEmpty())
		{
			requestBuilder.append(line + "\r\n");
		}
		String request = requestBuilder.toString();
		// Parse the request

		String[] requestsLines = request.split("\r\n");
		String[] requestLine = requestsLines[0].split(" ");
		String method = requestLine[0];
		String path = requestLine[1];
		String version = requestLine[2];
		String host = requestsLines[1].split(" ")[1];

		List<String> headers = new ArrayList<>();
		for (int h = 2; h < requestsLines.length; h++)
		{
			String header = requestsLines[h];
			headers.add(header);
		}

		String accessLog = String.format("Client %s, method %s, path %s, version %s, host %s, headers %s",
				client.toString(), method, path, version, host, headers.toString());
		System.out.println(accessLog);
		System.out.println(request);

		OutputStream clientOutput = client.getOutputStream();
		gui.getTextArea().appendText("Debug: got new client " + client.toString() + "\n");
		clientOutput.write("HTTP/1.1 200 OK\r\n".getBytes());
		clientOutput.write(("ContentType: text/html\r\n").getBytes());
		clientOutput.write("\r\n".getBytes());
		clientOutput.write("<b>It works!!</b>".getBytes());
		clientOutput.write("\r\n\r\n".getBytes());
		clientOutput.flush();
		client.close();

	}

}