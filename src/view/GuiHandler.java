package view;


import controller.Webserver;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;

/**
 *
 * @author leo
 */
public class GuiHandler
{
	// instance variabele
	private Label label;
	private TextArea textArea;
	private Button btnStop;
	private Thread thread;
	
	
	// constructor
	public GuiHandler(GridPane pane)
	{
		// default graphics settings  
        pane.setVgap(5);
        pane.setHgap(8);
        pane.setPadding(new Insets(10,15,20,15));
		
		label = new Label("HTTP Service available at port 8080");
		textArea = new TextArea();
		textArea.setMinHeight(200);
		textArea.setMinWidth(500);

		btnStop = new Button("Stop");
		
		btnStop.setOnAction(event -> stopApp());
		
		pane.add(label, 0, 0);
		pane.add(textArea, 0, 1);
		pane.add(btnStop, 0, 2);

		Webserver webServer = new Webserver(this);
	    thread = new Thread(webServer);
	    thread.start();
	}

	public Label getLabel()
	{
		return label;
	}

	public TextArea getTextArea()
	{
		return textArea;
	}
	
	private void stopApp()
	{
		thread.stop();
		System.exit(0);
	}
}
